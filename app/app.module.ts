import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './components/root_component/app.component';
import { TaskListComponent } from './components/task-list_component/task-list.component';
import { NewTaskFormComponent } from './components/new-task-form_component/new-task-form.component';
import { UserComponent } from './components/user_component/user.component';
import { SearchComponent } from './components/search_component/search.component'

import { TaskService } from './services/TaskService';
import { UserService } from './services/UserService';
import { TaskListService } from './services/TaskListService';

@NgModule({
    imports: [BrowserModule, HttpModule, FormsModule],
    declarations: [
        AppComponent, TaskListComponent, NewTaskFormComponent, UserComponent, SearchComponent
    ],
    bootstrap: [AppComponent],
    providers: [TaskService, UserService, TaskListService]
})
export class AppModule {

}