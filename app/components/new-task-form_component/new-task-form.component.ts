import { Component, Input } from '@angular/core';

import { TaskListService } from '../../services/TaskListService';

import { Task } from '../../model/Task';
import { TaskList } from '../../model/TaskList'
import { TaskImportance } from '../../model/TaskImportance';

@Component({
    moduleId: module.id,
    selector: 'new-task-form',
    templateUrl: 'new-task-form.component.html',
    styleUrls: ['new-task-form.component.css']
})
export class NewTaskFormComponent {
    taskList: TaskList;
    taskItemsCounter: number;
    showPopupWindow: boolean;

    constructor(private taskListService: TaskListService) {
        this.taskList = new TaskList();
        this.taskList.Tasks = new Array<Task>();
        this.taskItemsCounter = 0;
        this.taskList.Tasks[this.taskItemsCounter] = new Task();
        this.showPopupWindow = true;
    }

    submit(): void {
        this.taskListService.sendPostRequest(this.taskList).subscribe();
        console.log("Removed taskBar entity and service. TODO: Replace with TaskListService");

        this.togglePopUpWindow();
    }

    addNewTaskItem() {
        this.taskItemsCounter++;
    }

    togglePopUpWindow() {
        this.showPopupWindow = false;
    }
}