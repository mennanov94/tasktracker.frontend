import { Component } from '@angular/core';

import { Task } from '../../model/Task';
import { TaskService } from '../../services/TaskService';

@Component({
    moduleId: module.id,
    selector: 'task-list',
    templateUrl: 'task-list.component.html',
    styleUrls: ['task-list.component.css']
})
export class TaskListComponent {
    taskList: Array<Task>;
    constructor(private taskService: TaskService) {
        this.taskList = taskService.tasks;
    }
}