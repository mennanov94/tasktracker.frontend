import { Component } from '@angular/core';

import { User } from '../../model/User';
import { UserService } from '../../services/UserService';

@Component({
    moduleId: module.id,
    selector: 'user',
    templateUrl: 'user.component.html',
    styleUrls: ['user.component.css']
})
export class UserComponent {
    fullName: string;
    userIcon: string;

    constructor(private userService: UserService) {
        userService.getUser().subscribe(
            response => {
                this.fullName = response.FirstName + " " + response.LastName;
                this.userIcon = response.IconPath;
            },
            error => {
                console.log(error);
            }
        )
    }
}