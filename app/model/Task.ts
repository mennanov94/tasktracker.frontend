import { TaskImportance } from './TaskImportance';

export class Task{
    Id?: string;
    Title : string;
    IsCompleted : boolean;
    DueDate : Date;
    Importance : TaskImportance;
}