import { Task } from './Task';

export class TaskList{
    Id?: string;
    Title: string;
    UserId?: string;
    Tasks : Array<Task>;
}