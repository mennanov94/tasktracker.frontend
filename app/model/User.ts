import { TaskList } from './TaskList';

export class User {
    Id?: string;
    FirstName: string;
    LastName: string;
    IconPath: string;
    TaskLists?: Array<TaskList>
}