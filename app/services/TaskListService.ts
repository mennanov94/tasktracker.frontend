import { Injectable } from '@angular/core';
import { Http, Request, RequestOptions, Response, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { TaskList } from '../model/TaskList';

@Injectable()
export class TaskListService {
    private webApiUrl = 'http://localhost:4000/api/taskList';

    constructor(private http: Http) { }

    public sendPostRequest(taskList: TaskList): Observable<TaskList> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let requestOptions = new RequestOptions({ headers: headers });
        return this.http.post(this.webApiUrl, taskList, headers).map(this.parseSucces).catch(this.parseError);
    }

    private parseSucces(response: Response): Observable<TaskList> {
        return response.json() || {};
    }

    private parseError(response: Response): Observable<TaskList> {
        throw Observable.throw('Can not get data from server');
    }
}