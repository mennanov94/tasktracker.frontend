import { Injectable, Inject } from '@angular/core';
import { Http } from '@angular/http';

import { Task } from '../model/Task';
import { TaskImportance } from '../model/TaskImportance';
import { TaskWebApiUrl } from '../utils/utils'


@Injectable()
export class TaskService {
    constructor(private http: Http) { }

}