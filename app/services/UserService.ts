import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { User } from '../model/User';
import { UserWebApiUrl } from '../utils/utils';

@Injectable()
export class UserService {
    public user: User;

    constructor(private http: Http) { }

    public getUser(firstName: string, lastName: string) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let requestOptions = new RequestOptions({ headers: headers });

        this.http.get(UserWebApiUrl, )
    }
}