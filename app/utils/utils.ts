export const TaskListWebApiUrl: string = "http://localhost:4000/api/taskList";
export const TaskWebApiUrl: string = "http://localhost:4000/api/task/";
export const UserWebApiUrl: string = "http://localhost:4000/api/user/";
